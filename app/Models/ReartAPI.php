<?php

namespace App\Models;

use Illuminate\Support\Facades\Log;

class ReartAPI
{
    public static function getAPI($action, $params = null, $is_array = false)
    {
        $api = 'http://52.90.232.148:3000/api/';
        $return = null;
        $response = self::get($api.$action, $params);

        if ($response) {
            $all_data = json_decode($response, $is_array);

            if ($is_array) {
                if ($all_data && isset($all_data['data']) && ($all_data['status'] == '1' || $all_data['status'] == '200')) {
                    $return = $all_data['data'];
                } else {
                    Log::error('Api : '.$action.' error: ' . $all_data->message);
                }
            } else {
                if ($all_data && isset($all_data->data) && (($all_data->status == '1') || ($all_data->status == '200'))) {
                    $return = $all_data->data;
                } else {
                    Log::error('Api : '.$action.' error: ' . $all_data->message);
                }
            }
        }

        return $return;
    }

    public static function get($api_url = '', $params = null)
    {
        $url = $api_url . '?';

        if (isset($params)) {
            foreach ($params as $param => $value)
            {
                if ($value)
                    $url .= $param . '=' . $value . '&';
            }

            $url = substr($url, 0 , strlen($url) - 1);
        }

        return file_get_contents($url);
    }
}
