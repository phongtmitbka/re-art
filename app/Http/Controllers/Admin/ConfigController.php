<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Feature;
use App\Models\SystemInfo;
use App\Models\User;
use Illuminate\Http\Request;
use Mockery\Exception;

class ConfigController extends Controller
{
    public function index()
    {
        $configs = SystemInfo::all();

        return view('admin.index', compact('configs'));
    }

    public function feature()
    {
        $features = Feature::all();

        return view('admin.feature', compact('features'));
    }

    public function editFeature($id)
    {
        $feature = Feature::find($id);

        return view('admin.feature_edit', compact('feature'));
    }

    public function editConfig($id)
    {
        $config = SystemInfo::find($id);

        return view('admin.config_edit', compact('config'));
    }

    public function updateFeature($id, Request $request)
    {
        $config = Feature::find($id);
        $config->feature_name = $request->txtName;
        $config->description = $request->txtDescription;
        $config->save();

        return redirect(route('feature'));

    }

    public function updateConfig($id, Request $request)
    {
        $config = SystemInfo::find($id);
        $config->key_name = $request->txtKey;
        $config->type = $request->txtType;
        $config->name = $request->txtName;
        if ($config->type == 'image') {
            if ($request->hasFile('txtContent'))
            {
                $file = $request->file('txtContent');
                $file_extension = strtolower($file->getclientOriginalExtension());
                if ($file_extension == 'png' || $file_extension == 'jpg' || $file_extension == 'jpeg')
                {
                    $name = $file->getClientOriginalName();
                    $name = str_random(5)."_".$name;

                    try {
                        $file->move('upload', $name);
                        $config->content = 'upload/'.$name;
                    }
                    catch (Exception $e) {
                        echo $e;
                    }
                }
            }
        } else {
            $config->content = $request->txtContent;
        }

        $config->save();

        return redirect(route('config'));
    }
}
