<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ProfileRequest;

class ProfileController extends Controller
{
    public function edit()
    {
        $profile = Auth::user();

        return view('admin.profile.edit', compact('profile'));
    }


    public function update(ProfileRequest $request)
    {
        $profile = Auth::user();
        $profile->name = $request->txtName;

        if ($request->txtPass) {
            $profile->password = bcrypt($request->txtPass);
        }

        $profile->save();

        return view('admin.profile.edit', compact('profile'))->with('message', 'Change profile success');
    }
}
