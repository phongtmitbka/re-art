<footer class="site-footer" id="footer">
    <div class="container"><a class="logo" href="#"><img src="{{ $configs['logo'] }}" srcset="{{ $configs['logo'] }} 1x, re-art/app/imgs/logo@2x.png 2x" alt="Logo Re-Art"></a>
        <div class="about-us">ReX - AI Revolution<br>mail: {{ $configs['mail'] }}<br>Phone: {{ $configs['phone'] }}</div>
        <ul class="connect-us reset-list">
            <li><a href="{{ $configs['facebook'] }}">
                    <div class="fa fa-facebook"></div></a></li>
            <li><a href="{{ $configs['twitter'] }}">
                    <div class="fa fa-twitter"></div></a></li>
            <li><a href="{{ $configs['google'] }}">
                    <div class="fa fa-google-plus"></div></a></li>
            <li><a href="{{ $configs['pinterest'] }}">
                    <div class="fa fa-pinterest"></div></a></li>
        </ul>
        <div class="copyright">© 2017 <a href="Dore">Dore</a>{{ $configs['copy_right'] }}</div>
    </div>
</footer>
