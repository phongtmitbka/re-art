<div class="modal modal-full modal-pop-img-detail fade" id="popImgDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icon icon-cancel"></i></span></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="detail-img">
                    <div class="detail-heading">
                        <div class="fixed-column"><a class="btn btn-info btn-rounder btn-block btn-lg open-app" href="#">Open In App</a></div>
                        <div class="fluid-column">
                            <div class="inner">
                                <div class="media">
                                    <div class="media-left"><img src="re-art/app/imgs/samples/avt.png" alt="sample"></div>
                                    <div class="media-body">
                                        <div class="user-name">KimSoHyun</div>
                                        <div class="time">3 min ago</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="detail-content">
                        <p>There are days like that, quietly, not sad, not happy, slowly drift… the end of a day.</p>
                        <p><a href="#">#rainday<br></a><a href="#">#sad</a></p>
                        <div class="img-layout">
                            <div class="main-img"><img class="img-responsive" src="re-art/app/imgs/samples/pop-img-1.png" alt="sample"></div>
                            <div class="sub-img">
                                <div><img class="img-responsive" src="re-art/app/imgs/samples/pop-img-2.png" alt="sample"></div>
                                <div><img class="img-responsive" src="re-art/app/imgs/samples/pop-img-3.png" alt="sample"></div>
                            </div>
                        </div>
                    </div>
                    <div class="detail-footer">
                        <ul class="reset-list foot-actions">
                            <li><a href="#"><i class="icon icon-heart-o"></i><span class="likes">1266 likes</span></a></li>
                            <li><a href="#"><i class="icon icon-comment-o"></i><span class="likes">1266 conments</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
