<!-- build:js js/scripts.min.js-->
<script src="re-art/app/js/vendor/jquery.min.js"></script>
<script src="re-art/app/js/vendor/modernizr-2.8.3.min.js"></script>
<script src="re-art/app/js/bootstrap/bootstrap.min.js"></script>
<script src="re-art/app/js/vendor/jquery.easing.1.3.js"></script>
<script src="re-art/app/js/vendor/jquery.parallax-scroll.js"></script>
<script src="re-art/app/js/vendor/jquery.event.move.js"></script>
<script src="re-art/app/js/vendor/jquery.twentytwenty.js"></script>
<script src="re-art/app/js/vendor/slick.min.js"></script>
<script src="re-art/app/js/custom.js"></script>
<!-- endbuild-->