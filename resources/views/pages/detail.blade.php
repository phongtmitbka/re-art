@extends('layout.master')

@section('content')
    <main class="site-main" id="main">
        <div class="container-small">
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Library</a></li>
            </ol>
            <div class="detail-img-wrap">
                <div class="detail-heading">
                    <div class="fixed-column"><a class="btn btn-info btn-rounded btn-block btn-lg open-app" href="#">Open In App</a></div>
                    <div class="fluid-column">
                        <div class="inner">
                            <div class="media">
                                <div class="media-left"><img src="{{ $photo->avatar }}" class="img-circle" alt="sample" width="80px"></div>
                                <div class="media-body">
                                    <div class="user-name">{{ $photo->fullname }}</div>
                                    <div class="time">{{ $since }} ago</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="detail-content">
                    <p>There are days like that, quietly, not sad, not happy, slowly drift… the end of a day.</p>
                    <p><a href="#">#rainday<br></a><a href="#">#sad</a></p>
                    <div class="img-layout">
                        <div class="main-img">
                            <div class="ui-image"><img src="{{ $photo->output }}" alt="sample"></div>
                        </div>
                        <div class="sub-img">
                            <div>
                                <div class="ui-image"><img src="{{ $photo->input }}" alt="sample"></div>
                            </div>
                            <div>
                                <div class="ui-image"><img src="{{ $photo->template }}" alt="sample"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="detail-footer">
                    <ul class="reset-list foot-actions">
                        <li><a href="#"><i class="icon icon-heart-o"></i><span class="likes">{{ $photo->total_like }} likes</span></a></li>
                        <li><a href="#"><i class="icon icon-comment-o"></i><span class="likes">{{ $photo->total_comment }} comments</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </main>

@endsection
