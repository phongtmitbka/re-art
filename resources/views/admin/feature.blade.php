@extends('admin.layout.master')

@section('content')
    <div class="container-fluid">
        <div class="row col-lg-6">
            <div class="col-lg-12">
                <h1 class="page-header">Feature
                    <small>List</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr align="center">
                    <th>ID</th>
                    <th>Feature name</th>
                    <th>Description</th>
                    <th>Photo</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($features as $feature)
                        <tr class="odd gradeX" align="center">
                            <td>{{ $feature->id }}</td>
                            <td>{{ $feature->feature_name }}</td>
                            <td>{{ $feature->description }}</td>
                            <td><img src="{{ $feature->photo }}" alt="{{ $feature->feature_name }}" class="thumbnail"></td>
                            <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{{ route('editFeature', $feature->id) }}">Edit</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
@endsection