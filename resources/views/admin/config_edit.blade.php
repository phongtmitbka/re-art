@extends('admin.layout.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{ $config['name'] }}
                    <small>Edit</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <form action="{{ route('updateConfig', $config['id']) }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label>Key name</label>
                        <input type="text" class="form-control" name="txtKey" placeholder="Please Enter Name" value="{{ $config['key_name'] }}" required />
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="txtName" placeholder="Please Enter Name" value="{{ $config['name'] }}" required />
                    </div>
                    <div class="form-group">
                        <label>Type</label>
                        <select name="txtType" class="btn btn-default form-control" id="type">
                            <option value="image" {{ ($config['type'] == 'image') ? 'selected' : null }}>Image</option>
                            <option value="text" {{ ($config['type'] == 'text') ? 'selected' : null }}>Text</option>
                            <option value="string" {{ ($config['type'] == 'string') ? 'selected' : null }}>String</option>
                            <option value="email" {{ ($config['type'] == 'email') ? 'selected' : null }}>Email</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Content</label>
                        <div id="content">
                            @if($config['type'] == 'image')
                                <input type="file" name="txtContent" />
                            @elseif($config['type'] == 'text')
                                <textarea id="" cols="30" rows="10" class="form-control" name="txtContent" required>{{ $config['content'] }}</textarea>
                            @elseif($config['type'] == 'string')
                                <input type="text" class="form-control" placeholder="Please select content" name="txtContent" value="{{ $config['content'] }}" required>
                            @else
                                <input type="email" class="form-control" placeholder="Please select content" name="txtContent" value="{{ $config['content'] }}" required>
                            @endif
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default">Update</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>

    <script>
        $(document).ready(function() {
            $('#type').on('change', function () {
                $var = $(this).val();
                $('#content').empty();
                if ($var == 'string') {
                    $('#content').append('<input type="text" class="form-control" placeholder="Please select content" name="txtContent" value="{{ $config['content'] }}" required>');
                } else if ($var == 'text') {
                    $('#content').append('<textarea id="" cols="30" rows="10" class="form-control" name="txtContent" required>{{ $config['content'] }}</textarea>');
                } else if ($var == 'image') {
                    $('#content').append('<input type="file" name="txtContent" />');
                } else {
                    $('#content').append('<input type="email" class="form-control" placeholder="Please select content" name="txtContent" value="{{ $config['content'] }}" required>');
                }
            });
        });
    </script>
@endsection
