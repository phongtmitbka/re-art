@extends('admin.layout.master')

@section('content')
    <div class="container-fluid">
        <div class="row col-lg-12">
            <div class="col-lg-12">
                <h1 class="page-header">System Info
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr align="center">
                    <th>Key name</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Content</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($configs as $config)
                        <tr class="odd gradeX" align="center">
                            <td>{{ $config->key_name }}</td>
                            <td>{{ $config->name }}</td>
                            <td>{{ $config->type }}</td>
                            <td>
                                @if ($config->type == 'image')
                                    <img src="{{ $config->content }}" alt="{{ $config->name }}">
                                @else
                                    {{ $config->content }}
                                @endif
                            </td>
                            <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="{{ route('editConfig', $config->id) }}">Edit</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->
    </div>
@endsection
