<!DOCTYPE html>
<html lang="en">

@include('admin.layout.header')

<body>

<div id="wrapper">

    <!-- Navigation -->
@include('admin.include.menu')

<!-- Page Content -->
    <div id="page-wrapper">

        @yield('content')
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->

</body>

@include('admin.layout.footer')

</html>
