@extends('admin.layout.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Feature
                    <small>Edit</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                <form action="{{ route('updateFeature', $feature->id) }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label>Feature name</label>
                        <input type="text" class="form-control" name="txtName" value="{{ $feature->feature_name }}" placeholder="Please enter feature name" required />
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" class="form-control" name="txtDescription" value="{{ $feature->description }}" placeholder="Please enter description" required />
                    </div>
                    <div class="form-group">
                        <label>Photo</label>
                        <input type="file" name="txtPhoto" value="{{ $feature->photo }}" />
                    </div>
                    <button type="submit" class="btn btn-default">Update</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>
@endsection
