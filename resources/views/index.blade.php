@extends('layout.master')

@section('content')

  <body>
  @include('include.header')

  <main class="site-main" id="main">
    <div class="container">
      <section class="section-box download-sec"><a class="logo" href="#"><img src="{{ $configs['logo'] }}" srcset="{{ $configs['logo'] }} 1x, re-art/app/imgs/logo@2x.png 2x" alt="Logo Re-Art"></a>
        <div class="app-name"><img class="img-responsive" src="{{ $configs['logo'] }}" srcset="re-art/app/imgs/app-name.png 1x, re-art/app/imgs/app-name@2x.png 2x" alt="ReArt – Art Photo Editor"></div>
        <h3 class="sub-app-name">For photo and video</h3>
        <div class="show-app"><img class="img-responsive" src="{{ $configs['logo'] }}" srcset="re-art/app/imgs/show-app.png 1x, re-art/app/imgs/show-app@2x.png 2x" alt="Demo ReArt – Art Photo Editor"></div>
        <div class="download-buttons"><a class="button-play" href="{{ $configs['google_play'] }}"><img src="{{ $configs['logo'] }}" srcset="re-art/app/imgs/btn-google-play.png 1x, re-art/app/imgs/btn-google-play@2x.png 2x" alt="Download from Google Play Store"></a><a class="button-play" href="{{ $configs['app_store'] }}"><img src="{{ $configs['logo'] }}" srcset="re-art/app/imgs/btn-apple-store.png 1x, re-art/app/imgs/btn-apple-store@2x.png 2x" alt="Download from Apple Store"></a></div>
      </section>
      <section class="section-box section-text">
        <div class="box-border">
          <h3 class="section-title"><span>ABOUT APPS</span></h3>
          <p>{{ $configs['about'] }}</p>
        </div>
      </section>
      <section class="section-box feature-sec" id="feature-sec">
        <h3 class="section-title">FEATURES APP</h3>
        <div class="wrap-fea-effect">
          <div class="main-img"><img class="img-responsive" src="{{ $configs['logo'] }}" srcset="re-art/app/imgs/mobile-features.png 1x, re-art/app/imgs/mobile-features@2x.png 2x" alt="Logo Re-Art"></div>
          <ul class="list-features reset-list">
            <li class="fea-filter" data-parallax="{&quot;y&quot;: -250, &quot;from-scroll&quot;: 620, &quot;distance&quot;: 500, &quot;smoothness&quot;: 10}"><img src="{{ $features[0]->photo }}" alt="Filter - Modern art filter" class="thumbnail">
              <h4 class="fea-name">{{ $features[0]->feature_name }}</h4>
              <h5 class="fea-text">{{ $features[0]->description }}</h5>
            </li>
            <li class="fea-effects" data-parallax="{&quot;y&quot;: -250, &quot;from-scroll&quot;: 620, &quot;distance&quot;: 500, &quot;smoothness&quot;: 10}"><img src="{{ $features[1]->photo }}" alt="Effects - Stunning photo effect" class="thumbnail">
              <h4 class="fea-name">{{ $features[1]->feature_name }}</h4>
              <h5 class="fea-text">{{ $features[1]->description }}</h5>
            </li>
            <li class="fea-social" data-parallax="{&quot;y&quot;: -250, &quot;from-scroll&quot;: 620, &quot;distance&quot;: 500, &quot;smoothness&quot;: 10}"><img src="{{ $features[2]->photo }}" alt="Social - Instant share photos" class="thumbnail">
              <h4 class="fea-name">{{ $features[2]->feature_name }}</h4>
              <h5 class="fea-text">{{ $features[2]->description }}</h5>
            </li>
            <li class="fea-discover" data-parallax="{&quot;y&quot;: -250, &quot;from-scroll&quot;: 620, &quot;distance&quot;: 500, &quot;smoothness&quot;: 10}"><img src="{{ $features[3]->photo }}" alt="Discover - Like anh follow" class="thumbnail">
              <h4 class="fea-name">{{ $features[3]->feature_name }}</h4>
              <h5 class="fea-text">{{ $features[3]->description }}</h5>
            </li>
            <li class="fea-share" data-parallax="{&quot;y&quot;: -250, &quot;from-scroll&quot;: 620, &quot;distance&quot;: 500, &quot;smoothness&quot;: 10}"><img src="{{ $features[4]->photo }}" alt="Share - Fast Sharing" class="thumbnail">
              <h4 class="fea-name">{{ $features[4]->feature_name }}</h4>
              <h5 class="fea-text">{{ $features[4]->description }}</h5>
            </li>
            <li class="fea-edit" data-parallax="{&quot;y&quot;: -250, &quot;from-scroll&quot;: 620, &quot;distance&quot;: 500, &quot;smoothness&quot;: 10}"><img src="{{ $features[5]->photo }}" alt="Edit - Edit and share photo" class="thumbnail">
              <h4 class="fea-name">{{ $features[5]->feature_name }}</h4>
              <h5 class="fea-text">{{ $features[5]->description }}</h5>
            </li>
          </ul>
        </div>
      </section>
      <section class="section-box section-text">
        <div class="box-border">
          <h3 class="section-title"><span>MESSAGE</span></h3>
          <p>{{ $configs['message'] }}</p>
        </div>
      </section>
      <section class="section-box top-style-sec" id="style-sec">
        <div class="box-border">
          <h3 class="section-title">TOP STYLES</h3>
          <div class="style-slider">
            @foreach($styles as $style)
              <div>
                <div class="style-item"><img class="img-responsive styles-photo" src="{{ $style->photo }}"
                                             alt="{{ $style->title }}"><span class="style-name">{{ $style->title }}</span></div>
              </div>
            @endforeach
          </div>
        </div>
      </section>
      <section class="section-box popular-img-sec" id="popular-img-sec">
        <div class="box-border">
          <h3 class="section-title">POPULAR IMAGE</h3>
          <div class="row popular-img-layout">
            @foreach($photos as $photo)
              <div class="col-md-3 col-sm-4 col-xs-6">
                <div class="compare-item"><img class="preview photo-image" src="{{ $photo->input }}"
                                               alt="sample">
                  <div class="likes"><a href="#"><i class="fa fa-heart-o"></i><strong>{{ $photo->total_like }}
                        &nbsp;</strong><span>likes</span></a></div>
                  <a class="btn btn-xs btn-primary show-detail popImgDetail" href="{{ route('detail', $photo->id) }}" data-toggle="modal" target="_blank">Detail</a>
                  <div class="compare-image"><img class="photo-image" src="{{ $photo->input }}"
                                                  alt="sample"><img src="{{ $photo->output }}"
                                                                    class="photo-image" alt="sample"></div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </section>
    </div>
  </main>

  @include('include.footer')

  @include('include.detail')

  </body>
@endsection
